<?php

// Ottieni la variabile 'redirect' tramite GET
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : '/';

// Richiedi l'autoloader per le classi
require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/autoload.inc.php';

// Crea un oggetto per il Controller
$controller = new Utente\UtenteContr();

// Esegui il logout
$controller->logout();

// Manda l'utente alla pagina del redirect
header('Location: ' . $redirect);
