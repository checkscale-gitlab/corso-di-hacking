<?php

// Se l'utente ha raggiunto questa pagina senza impostare le varibili POST (ad
// esempio digitando l'URL manualmente), riportalo alla pagina di login
if (empty($_POST)) {
	header('Location: ./');
	exit();
}

// Ottieni la variabile 'redirect' tramite GET e le credenziali tramite POST
$redirect   = isset($_GET['redirect'])    ? $_GET['redirect']              : '/';
$username   = isset($_POST['username'])   ? strtolower($_POST['username']) : null;
$password   = isset($_POST['password'])   ? $_POST['password']             : null;

// Se le variabili '$username' e '$password' sono vuote, ritorna un errore
if (empty($username) || empty($password)) {
	$_SESSION['errore'] = 'Riempi tutti i campi e riprova';
	header('Location: ./');
	exit();
}

// Richiedi l'autoloader per le classi
require_once $_SERVER['DOCUMENT_ROOT'] . '/../src/autoload.inc.php';

// Crea un oggetto per il Controller
$controller = new Utente\UtenteContr($username);

// Se lo username non esiste nel database, ritorna un errore
if (!$controller->esistenzaUtente()) {
	$_SESSION['errore'] = 'Username non esistente';
	header('Location: ./');
	exit();
}

// Se la password è errata, ritorna un errore
if (!$controller->verificaPassword($password)) {
	$_SESSION['errore'] = 'Password errata, riprova';
	header('Location: ./');
	exit();
}

// Imposta le variabili di sessione
$controller->login();

// Manda l'utente alla pagina del redirect
header('Location: ' . $redirect);
