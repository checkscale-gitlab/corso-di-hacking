<?php

class DbHandler {
	/**
	 * Connettiti al database e ritorna la chiave di connessione al database
	 *
	 * @return PDO
	 */
	final protected function connessione() {
		// Definisci il path del database
		$path = $_SERVER['DOCUMENT_ROOT'] . '/../database/database.sqlite3';

		// Prova a connetterti al database e, se la connessione fallisce,
		// ritorna un errore
		try {
			$db = new PDO('sqlite:' . $path);
		} catch (PDOException $errore) {
			echo 'Errore di connessione al database<br><br>Metodo: ' . __METHOD__ . '()';
			exit();
		}

		// Fai in modo che le query ritornino un array associativo
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

		// Ritorna la chiave di connessione al database
		return $db;
	}
}
