<?php

namespace Utente;

class UtenteContr extends UtenteModel {
	private $username;
	private $nome;
	private $cognome;
	private $ruolo;

	/**
	 * Costruttore: Cerca un utente nel database e, se lo trova, imposta gli
	 * attributi della classe
	 *
	 * @param  string $username Lo username dell'utente
	 * @return null
	 */
	public function __construct(string $username = null) {
		// Preleva i dati dell'utente dal database
		$result = isset($username) ? $this->ottieniUtente($username) : null;

		// Se l'utente esiste, imposta gli attributi della classe
		if ($result) {
			$this->username = $username;
			$this->nome     = $result['nome'];
			$this->cognome  = $result['cognome'];
			$this->ruolo    = $result['ruolo'];
		}
	}

	/**
	 * Verifica che l'utente esista
	 *
	 * @return bool
	 */
	public function esistenzaUtente() {
		return isset($this->username);
	}

	/**
	 * Verifica che l'hash della password inserita dall'utente e l'hash della
	 * password salvata nel database combacino
	 *
	 * @param  string $password La password da verificare
	 * @return bool
	 */
	public function verificaPassword(string $password) {
		// Preleva i dati dell'utente dal database
		$result = $this->ottieniUtente($this->username);

		// Compara la password inserita con quella nel database
		return (bool) password_verify($password, $result['password']);
	}

	/**
	 * Imposta le variabili di sessione
	 *
	 * @return null
	 */
	public function login() {
		$_SESSION['username'] = $this->username;
		$_SESSION['nome']     = $this->nome;
		$_SESSION['cognome']  = $this->cognome;
		$_SESSION['ruolo']    = $this->ruolo;
	}

	/**
	 * Elimina la sessione
	 *
	 * @return null
	 */
	public function logout() {
		// Libera tutte le variabili di sessione
		session_unset();

		// Distruggi la sessione
		session_destroy();
	}
}
